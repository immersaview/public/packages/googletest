**Nuget packaging for GoogleTest Repository**

## Example CMake Usage:
```cmake
target_link_libraries(target gtest gtest_main)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:gtest,INTERFACE_INCLUDE_DIRECTORIES>")
```
etc.