#!/usr/bin/env bash

# Usage:
# build <generator> <arch> <config> <OS>

# Exit as failure if any command fails
set -e

if [[ -z "$1" || "$1" == "-h" || "$1" == "--help" ]]; then
    echo "$0 <generator> <arch> <config> <OS>"
    exit 0
fi

BASE_DIR="`cd \`dirname ${BASH_SOURCE}\` && pwd`"
GENERATOR="$1"
ARCH="$2"
CONFIG="$3"
OS="$4"

CMAKE_ARGS="-DCMAKE_BUILD_TYPE=${CONFIG}"

if [[ "${GENERATOR}" == "Visual Studio"* ]]; then
    if [[ "${ARCH}" == "x86" ]]; then
        CMAKE_ARGS="${CMAKE_ARGS} -A Win32"
    else
        CMAKE_ARGS="${CMAKE_ARGS} -A ${ARCH}"
    fi

    BUILD_DIR="${BASE_DIR}/${GENERATOR} ${ARCH}/MD"
    BUILD_ARGS="/consoleloggerparameters:ForceConsoleColor /maxcpucount:`nproc`"
    CMAKE_ARGS_MD="${CMAKE_ARGS} -Dgtest_force_shared_crt=1"

    rm -rf "${BUILD_DIR}"
    mkdir -p "${BUILD_DIR}"

    pushd "${BUILD_DIR}"

        cmake -G "${GENERATOR}" ${CMAKE_ARGS_MD} "${BASE_DIR}"
        cmake --build . --config ${CONFIG} -- ${BUILD_ARGS}

    popd
    
    BUILD_DIR="${BASE_DIR}/${GENERATOR} ${ARCH}/MT"
    CMAKE_ARGS_MT="${CMAKE_ARGS} -Dgtest_force_shared_crt=0"
    
    rm -rf "${BUILD_DIR}"
    mkdir -p "${BUILD_DIR}"

    pushd "${BUILD_DIR}"

        cmake -G "${GENERATOR}" ${CMAKE_ARGS_MT} "${BASE_DIR}"
        cmake --build . --config ${CONFIG} -- ${BUILD_ARGS}

    popd
    
elif [[ "${GENERATOR}" == "Unix Makefiles" ]]; then
    BUILD_DIR="${BASE_DIR}/${OS} ${GENERATOR} ${ARCH}/${CONFIG}"
    BUILD_ARGS="-j `nproc` VERBOSE=1"
    
    rm -rf "${BUILD_DIR}"
    mkdir -p "${BUILD_DIR}"

    pushd "${BUILD_DIR}"

        cmake -G "${GENERATOR}" ${CMAKE_ARGS} "${BASE_DIR}"
        cmake --build . --config ${CONFIG} -- ${BUILD_ARGS}

    popd
else
    echo "Unsupported generator"
    exit 1
fi
